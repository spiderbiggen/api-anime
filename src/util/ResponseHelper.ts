import { ErrorResponse, HttpError } from './Errors';
import { Request, Response } from 'express';

/**
 * Holds helper methods for sending a response to the client.
 *
 * @author Stefan Breetveld
 */
export module ResponseHelper {

  /**
   * Send a successful response to the client with the given data.
   *
   * @param req the Request sent by the client
   * @param res the Response we need to send responses to
   * @param data any object that needs to be sent to the client
   */
  export function sendResponse(req: Request, res: Response, data: any) {
    if (Array.isArray(data)) {
      data = {
        results: data,
        count: data.length,
      };
    }
    const message = { status: 'success', data: data };
    send(res, 200, message);
  }

  /**
   * Send an error response to the client with the given error.
   *
   * @param req the Request sent by the client.
   * @param res the Response we need to send responses to
   * @param error the error that has occurred.
   */
  export function sendError(req: Request, res: Response, error: HttpError) {
    const errorResponse: ErrorResponse = error.error(req);
    const errorMessage = errorResponse.data;
    const status = errorResponse.status;
    send(res, status, errorMessage);
  }

  /**
   * Send a json message to the client with the specified status code.
   *
   * @param res the Response we need to send responses to
   * @param status the http status code
   * @param message the object that should be sent to the client
   */
  function send(res: Response, status: number, message: any) {
    res.status(status).json(message);
  }
}
