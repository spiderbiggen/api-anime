const tagsToReplace: { [key: string]: string } = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
};
const pattern = new RegExp(`[${Object.keys(tagsToReplace).join()}]`, 'g');

export function escape(text: string): string {
  return text.replace(new RegExp(pattern, 'g'), tag => tagsToReplace[tag] ?? tag);
}

const BASE64_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

export function generateBase64Salt(length: number): string {
  let result = '';
  do {
    result += BASE64_CHARS.charAt(Math.floor(Math.random() * BASE64_CHARS.length));
  } while (result.length != length);
  return result;
}

export function toBoolean(value?: string | number): boolean {
  if (!value) {
    return false;
  }
  if (typeof value === 'string') {
    value = value.toLowerCase();
  }
  switch (value) {
    case 'y':
    case 'yes':
    case 't':
    case 'true':
    case '1':
      return true;
    default:
      return false;
  }
}

/**
 * Calculate levenshtein distance between two strings
 * @see https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#JavaScript
 * @param a source string
 * @param b target string
 */
export function computeLevenshteinDistance(a: string, b: string): number {
  if (a.length === 0) return b.length;
  if (b.length === 0) return a.length;

  const matrix = [];

  // increment along the first column of each row
  for (let i = 0; i <= b.length; i++) {
    matrix[i] = [i];
  }

  // increment each column in the first row
  for (let j = 0; j <= a.length; j++) {
    matrix[0][j] = j;
  }

  // Fill in the rest of the matrix
  for (let i = 1; i <= b.length; i++) {
    for (let j = 1; j <= a.length; j++) {
      if (b.charCodeAt(i - 1) === a.charCodeAt(j - 1)) {
        matrix[i][j] = matrix[i - 1][j - 1];
      } else {
        matrix[i][j] = Math.min(matrix[i - 1][j - 1] + 1, // substitution
          Math.min(matrix[i][j - 1] + 1, // insertion
            matrix[i - 1][j] + 1)); // deletion
      }
    }
  }

  return matrix[b.length][a.length];
}

export function computePercentageLevenshteinDistance(source?: string, target?: string): number {
  if (!source || !target) return 0;
  if (source === target) return 1;
  const distance = computeLevenshteinDistance(source, target);
  return (1 - distance / Math.max(source.length, target.length));
}
