import {Request} from 'express';

/**
 * The base class for all HTTP Errors
 *
 * @author Stefan Breetveld
 */
export abstract class HttpError {

  readonly message?: string;
  readonly status: number;

  /**
   * Constructs a new instance with the given data.
   *
   * @param message the reason this error occurred
   * @param extra any extra data.
   * @param status the http status code
   */
  protected constructor(message?: string, protected extra?: { [key: string]: any }, status: number = 500) {
    this.message = message;
    this.status = status;
  }

  /**
   * Map the error to an ErrorResponse Object.
   *
   * @param req the request sent by the client
   */
  error(req: Request): ErrorResponse {
    const obj: any = this.extra || {};
    obj.message = this.message;
    obj.status = 'error';
    return {data: obj, status: this.status};
  }

}

/**
 * HTTP status 400. Any request that doesn't get the correct parameters from the client.
 */
export class BadRequestError extends HttpError {
  constructor(message?: string, extra?: { [p: string]: any }) {
    super(message, extra, 400);
  }
}

/**
 * HTTP status 404. Any request that requests a resource that doesn't exist.
 */
export class NotFoundError extends HttpError {
  constructor(extra?: { [p: string]: any }) {
    super(undefined, extra, 404);
  }

  /**
   * Map the error to an ErrorResponse Object.
   *
   * @param req the request sent by the client
   */
  error(req: Request): ErrorResponse {
    const res = super.error(req);
    res.data.reason = `${req.originalUrl} wasn't found`;
    return res;
  }
}

/**
 * HTTP status 500. Any server side error.
 */
export class GenericServerError extends HttpError {
  constructor(extra?: { [p: string]: any }) {
    super(undefined, extra);
  }

  /**
   * Map the error to an ErrorResponse Object.
   *
   * @param req the request sent by the client
   */
  error(req: Request): ErrorResponse {
    const res = super.error(req);
    res.data.reason = `${req.originalUrl} caused an error!`;
    return res;
  }
}

/**
 * HTTP status 409. Any time a request is sent that tries to add a new object that already exists.
 */
export class DuplicateObjectError extends HttpError {
  duplicate: any;

  constructor(obj: any, message?: string, extra?: { [p: string]: any }) {
    super(message, extra, 409);
    this.duplicate = obj;
  }

  /**
   * Map the error to an ErrorResponse Object.
   *
   * @param req the request sent by the client
   */
  error(req: Request): ErrorResponse {
    const response: ErrorResponse = super.error(req);
    response.data.reason = 'Tried to add duplicate object';
    response.data.duplicate = this.duplicate;
    return response;
  }
}

/**
 * HTTP status 401. Any request that is sent without authentication while it is required.
 */
export class UnauthorizedError extends HttpError {

  constructor(message?: string, extra?: { [p: string]: any }) {
    super(message, extra, 401);
  }

  /**
   * Map the error to an ErrorResponse Object.
   *
   * @param req the request sent by the client
   */
  error(req: Request): ErrorResponse {
    const response: ErrorResponse = super.error(req);
    response.data.reason = 'Unauthorized Access';
    return response;
  }
}

/**
 * HTTP status 403. Any request that requires different authentication than the user has.
 */
export class ForbiddenError extends HttpError {

  constructor(message?: string, extra?: { [p: string]: any }) {
    super(message, extra, 403);
  }

  /**
   * Map the error to an ErrorResponse Object.
   *
   * @param req the request sent by the client
   */
  error(req: Request): ErrorResponse {
    const response: ErrorResponse = super.error(req);
    response.data.reason = 'Forbidden Access';
    return response;
  }
}

/**
 * Defines the error response object.
 */
export class ErrorResponse {
  status: number;
  data: {
    reason?: string;
    [key: string]: any
  };
}
