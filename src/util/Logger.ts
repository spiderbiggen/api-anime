import { createLogger, format, transports } from 'winston';

export const DEBUG = 'debug';
export const VERBOSE = 'verbose';
export const INFO = 'info';
export const WARN = 'warn';
export const ERROR = 'error';

const colorizer = format.colorize({
  colors: {
    debug: 'green',
    verbose: 'gray',
    info: 'white',
    warn: 'yellow',
    error: 'red',
  },
});

// const myFormat = format.printf(  ({ level, message, timestamp }) => colorizer.colorize(level, `${ timestamp } ${ level }: ${ message }`));
//
// const formatters = {
//   default: format.combine(
//     format.timestamp(),
//     format.splat(),
//     format.printf(({ level, message, timestamp }) => `${ timestamp } ${ level }: ${ message }`),
//   ),
//   console:
//
// };

const printf = format.printf(({ level, message, timestamp }) => `${ timestamp } ${ level }: ${ message }`);
const colored = format.printf(({ level, message, timestamp }) => colorizer.colorize(level, `${ timestamp } ${ level }: ${ message }`));
const myFormat = format.combine(
  format.timestamp(),
  format.splat(),
);


export const LOGGER = createLogger({
    level: DEBUG,
    format: format.combine(myFormat, printf),
    defaultMeta: { service: 'user-service' },
    transports: [
      new transports.Console({ level: VERBOSE, format: format.combine(myFormat, colored) }),

      // - Write to all logs with level `info` and below to `combined.log`
      new transports.File({ filename: 'error.log', level: ERROR }),

      // - Write all logs error (and below) to `error.log`.
      new transports.File({ filename: 'combined.log', level: INFO }),
    ],
  })
;
