import {sign, verify} from 'jsonwebtoken';
import {generateBase64Salt, ResponseHelper, UnauthorizedError} from './index';
import {NextFunction, Request, Response} from 'express-serve-static-core';

const SECRET = process.env.JWT_SECRET || 'BEST_SECRET_EU';
/** Default to 5 minutes, in seconds */
const EXPIRES_IN: number = Number(process.env.EXPIRES_IN) || 300;
/** Defaults to 30 days, in seconds*/
const REFRESH_EXPIRES_IN: number = Number(process.env.REFRESH_EXPIRES_IN) || 2_592_000;

/**
 * Module that holds all functions related to authentication.
 *
 * @author Stefan Breetveld
 */
export module Authentication {

  /**
   * Verify if the given token is valid and decrypt it.
   * @param token a possibly valid jwt token.
   *
   * @throws {@link VerifyErrors} when an error occurs during verification.
   * @return The decrypted token.
   */
  export function verifyToken(token: string): Promise<TokenDetails> {
    return new Promise((resolve, reject) => {
      verify(token, SECRET, (err, decoded) => {
        if (err) {
          reject(err);
        } else {
          resolve(decoded as TokenDetails);
        }
      });
    });
  }

  /**
   * Sign TokenDetails as a valid jwt token ready to be sent to the end user.
   * @param details any details you want to send back to the user
   * @return signed jwt token
   */
  export function signToken(details: TokenDetails | RefreshTokenDetails): string {
    return sign({maxAge: details.maxAge, sessionData: details.sessionData}, SECRET, {
      algorithm: 'HS256',
      expiresIn: details.maxAge,
    });
  }

  /**
   * Verify if a given request is valid and if sent the request further along the call chain.
   * When a request is not properly authorized the request will stop here and respond with and appropriate unauthorized
   * response code
   * @param req Any received request
   * @param res Where the possible Unauthorized response will be sent
   * @param next the next piece of middleware or the requested resource
   */
  export function verifyAuthentication(req: Request, res: Response, next: NextFunction) {
    if (res.locals.user) {
      next();
    } else {
      ResponseHelper.sendError(req, res, new UnauthorizedError('No valid token found.'));
    }
  }

  export async function userMiddleware(req: Request, res: Response, next: NextFunction) {
    const authHeader: string = req.header('authorization') || '';
    const parts = authHeader.split(' ');
    try {
      if (authHeader.toLowerCase().startsWith('bearer') && parts.length === 2) {
        const token = parts[1];
        const decodedToken = await verifyToken(token);
        res.locals.user = decodedToken.sessionData.id;
      }
    } catch (e) {
      // Can't find user if there is no token
    }
    next();
  }
}

/**
 * Ensure correct Jsonification of Token Payload
 */
export type TokenPayload = {
  token: string;
  refresh_token?: string;
}

/**
 * Hold any data that should be included in the token as part of the payload.
 *
 * @author Stefan Breetveld
 */
export type SessionData = {
  id: string;
}

export class TokenDetails {
  readonly maxAge: number;
  readonly sessionData: SessionData;

  /**
   * Create a new instance of this class.
   *
   * @param sessionData any data that should be included in the token.
   * @param maxAge the total time this token should be valid in milli-seconds
   */
  constructor(sessionData: SessionData, maxAge: number = EXPIRES_IN) {
    this.maxAge = maxAge;
    this.sessionData = sessionData;
  }
}

export class RefreshTokenDetails {
  readonly sessionData: SessionData & { expires: number, salt: string };
  readonly maxAge: number;
  readonly salt: string;

  /**
   * Create a new instance of this class.
   *
   * @param sessionData any data that should be included in the token.
   * @param maxAge the total time this token should be valid in milli-seconds
   */
  constructor(sessionData: SessionData, maxAge: number = REFRESH_EXPIRES_IN) {
    this.maxAge = maxAge;
    this.sessionData = Object.assign({}, sessionData, {expires: maxAge, salt: generateBase64Salt(20)});
  }
}

