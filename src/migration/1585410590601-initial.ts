import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1585410590601 implements MigrationInterface {
    name = 'initial1585410590601'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "episode" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "number" integer NOT NULL, "title" character varying NOT NULL, "title_japanese" character varying NOT NULL, "title_romanji" character varying NOT NULL, "title_english" character varying NOT NULL, "aired" TIMESTAMP NOT NULL, "animeId" uuid, CONSTRAINT "PK_7258b95d6d2bf7f621845a0e143" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_7cfecbd114ad6bebfb338eeff0" ON "episode" ("animeId", "number") `, undefined);
        await queryRunner.query(`CREATE TABLE "url" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "url" character varying NOT NULL, "type" character varying NOT NULL, "resolution" integer, "animeId" uuid, "episodeId" uuid, CONSTRAINT "PK_7421088122ee64b55556dfc3a91" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_e959200334338eda88b0137c98" ON "url" ("animeId", "episodeId") `, undefined);
        await queryRunner.query(`CREATE TABLE "anime" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "mal_id" integer NOT NULL, "title" character varying NOT NULL, "title_english" character varying, "title_japanese" character varying, "synopsis" character varying, "image" character varying, "from" TIMESTAMP, "to" TIMESTAMP, "mal_url" character varying NOT NULL, "animeflix_slug" character varying, "animeflix_id" integer, CONSTRAINT "UQ_a0e2671f2b58abd3651972651c1" UNIQUE ("mal_id"), CONSTRAINT "PK_6e567f73ed63fd388a7734cbdd3" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "refresh_token" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "token" character varying(512) NOT NULL, "expires" TIMESTAMP NOT NULL, "disabled" boolean NOT NULL DEFAULT false, "user_id" uuid, CONSTRAINT "UQ_c31d0a2f38e6e99110df62ab0af" UNIQUE ("token"), CONSTRAINT "PK_b575dd3c21fb0831013c909e7fe" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "username" character varying NOT NULL, "mal_username" character varying, "email" character varying NOT NULL, "password" character varying NOT NULL, "roles" text NOT NULL, CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE ("username"), CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`ALTER TABLE "episode" ADD CONSTRAINT "FK_969247f869e17aeb3bcd3c548f6" FOREIGN KEY ("animeId") REFERENCES "anime"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "url" ADD CONSTRAINT "FK_4c85c74484faad314ed971b471d" FOREIGN KEY ("animeId") REFERENCES "anime"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "url" ADD CONSTRAINT "FK_79d26d3bbf0450ab135252ab820" FOREIGN KEY ("episodeId") REFERENCES "episode"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "refresh_token" ADD CONSTRAINT "FK_6bbe63d2fe75e7f0ba1710351d4" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`CREATE TABLE "query-result-cache" ("id" SERIAL NOT NULL, "identifier" character varying, "time" bigint NOT NULL, "duration" integer NOT NULL, "query" text NOT NULL, "result" text NOT NULL, CONSTRAINT "PK_6a98f758d8bfd010e7e10ffd3d3" PRIMARY KEY ("id"))`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE "query-result-cache"`, undefined);
        await queryRunner.query(`ALTER TABLE "refresh_token" DROP CONSTRAINT "FK_6bbe63d2fe75e7f0ba1710351d4"`, undefined);
        await queryRunner.query(`ALTER TABLE "url" DROP CONSTRAINT "FK_79d26d3bbf0450ab135252ab820"`, undefined);
        await queryRunner.query(`ALTER TABLE "url" DROP CONSTRAINT "FK_4c85c74484faad314ed971b471d"`, undefined);
        await queryRunner.query(`ALTER TABLE "episode" DROP CONSTRAINT "FK_969247f869e17aeb3bcd3c548f6"`, undefined);
        await queryRunner.query(`DROP TABLE "user"`, undefined);
        await queryRunner.query(`DROP TABLE "refresh_token"`, undefined);
        await queryRunner.query(`DROP TABLE "anime"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_e959200334338eda88b0137c98"`, undefined);
        await queryRunner.query(`DROP TABLE "url"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_7cfecbd114ad6bebfb338eeff0"`, undefined);
        await queryRunner.query(`DROP TABLE "episode"`, undefined);
    }

}
