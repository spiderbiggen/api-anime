import {UserController} from '../controllers';
import {RouteWrapper} from '../../routing';
import {Authentication} from '../../util';

const router = new RouteWrapper('/users');

router.post('/authenticate', UserController.loginUser);
router.post('/register', UserController.register);
router.post('/refresh', UserController.refresh);
const authenticatedRouter = new RouteWrapper('');

authenticatedRouter.registerMiddleware(Authentication.verifyAuthentication);
authenticatedRouter.get(/\/(me|self|session|whoami)$/, UserController.self);
router.subRoutes(authenticatedRouter);

export default router;
