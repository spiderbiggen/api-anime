import UserRoutes from './UserRoutes';
import AnimeRoutes from './AnimeRoutes';

/**
 * Export a list of all routes
 */
export default [UserRoutes, AnimeRoutes];
