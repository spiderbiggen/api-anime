import { RouteWrapper } from '../../routing';
import { AnimeController } from '../controllers/AnimeController';
import { NyaaController } from '../controllers/NyaaController';

const router = new RouteWrapper('/anime');

router.get('/search', AnimeController.find);
router.get('/top', AnimeController.getPopular);
router.get('/nyaa', NyaaController.getXPages);
router.get('/:id', AnimeController.get);

export default router;
