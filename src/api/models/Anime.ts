import { AfterLoad, Column, Entity, getRepository, OneToMany } from 'typeorm';
import { DatedEntity } from './DatedEntity';
import { Url } from './Url';
import { Episode } from './Episode';

const MAX_SHORT_SYNOPSIS = 100;

/**
 * Entity that holds information about an anime.
 *
 * @author Stefan Breetveld
 */
@Entity()
export class Anime extends DatedEntity {

  @Column('integer', { unique: true })
  mal_id: number;
  @Column()
  title: string;
  @Column({ nullable: true })
  title_english?: string;
  @Column({ nullable: true })
  title_japanese?: string;
  @Column({ nullable: true })
  synopsis?: string;
  @Column({ nullable: true })
  image?: string;
  @Column({ nullable: true })
  from?: Date;
  @Column('timestamp', { nullable: true })
  to?: Date;
  @Column()
  mal_url: string;
  @Column({ nullable: true })
  animeflix_slug?: string;
  @Column({ nullable: true })
  animeflix_id?: number;
  @OneToMany(() => Url, url => url.anime)
  urls: Url[];
  @OneToMany(() => Episode, episode => episode.anime)
  episodes: Episode[];
  short_synopsis?: string;

  @AfterLoad()
  afterLoad() {
    if (!this.synopsis) return;
    if (this.synopsis.length > MAX_SHORT_SYNOPSIS) {
      this.short_synopsis = this.synopsis.substring(0, this.synopsis.lastIndexOf(' ', MAX_SHORT_SYNOPSIS - 3)) + '...';
    }
  }
}

export function getAnimeRepository() {
  return getRepository(Anime);
}
