import { Column, Entity, getRepository, Index, ManyToOne } from 'typeorm';
import { DatedEntity } from './DatedEntity';
import { Anime } from './Anime';
import { Episode } from './Episode';

/**
 * Entity that holds information about a url.
 *
 * @author Stefan Breetveld
 */
@Entity()
@Index(['anime', 'episode'], { unique: true })
export class Url extends DatedEntity {
  @Column({ nullable: false, select: false })
  url: string; // technically a hash of the password.
  @ManyToOne(() => Anime, anime => anime.urls)
  anime: Anime;
  @ManyToOne(() => Episode, episode => episode.urls)
  episode: Episode;
  @Column()
  type: string;
  @Column('integer', { nullable: true })
  resolution: number;
}

export function getUrlRepository() {
  return getRepository(Url);
}
