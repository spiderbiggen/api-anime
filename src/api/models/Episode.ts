import { Column, Entity, getRepository, Index, ManyToOne, OneToMany } from 'typeorm';
import { DatedEntity } from './DatedEntity';
import { Url } from './Url';
import { Anime } from './Anime';

/**
 * Entity that holds information about a episode.
 *
 * @author Stefan Breetveld
 */
@Entity()
@Index(['anime', 'number'], { unique: true })
export class Episode extends DatedEntity {

  @ManyToOne(() => Anime, anime => anime.episodes)
  anime: Anime;
  @Column('integer')
  number: number;
  @Column()
  title: string;
  @Column()
  title_japanese: string;
  @Column()
  title_romanji: string;
  @Column()
  title_english: string;
  @Column()
  aired: Date;
  @OneToMany(() => Url, url => url.episode)
  urls: Url[]; // technically a hash of the password.
}

export function getEpisodeRepository() {
  return getRepository(Episode);
}
