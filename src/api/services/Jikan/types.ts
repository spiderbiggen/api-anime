type JikanResponse = {
  request_hash: string
  request_cached: boolean,
  request_cache_expiry: number,
}

export type AnimeResponse = JikanResponse & {
  mal_id: number,
  url: string,
  image_url: string,
  trailer_url: string,
  title: string,
  title_english?: string,
  title_japanese?: string,
  title_romanji?: string,
  title_synonyms?: string[],
  type: string,
  source: string,
  episodes: number,
  status: string,
  airing: boolean,
  aired: {
    from: string,
    to: string,
    prop: {
      from: {
        day: number,
        month: number,
        year: number
      },
      to: {
        day: number,
        month: number,
        year: number
      }
    },
    string: string
  },
  duration: string,
  rating: string,
  score: number,
  scored_by: number,
  rank: number,
  popularity: number,
  members: number,
  favorites: number,
  synopsis: string,
  background: string,
  premiered: string,
  broadcast: string,
  related: {
    'Adaptation': MalReference[],
    'Side story': MalReference[],
    'Summary': MalReference[]
    [key: string]: MalReference[]
  },
  producers: MalReference[],
  licensors: MalReference[],
  studios: MalReference[],
  genres: MalReference[],
  opening_themes: string[],
  ending_themes: string[],
}

export type EpisodeResponse = JikanResponse & {
  episodes_last_page: number,
  episodes: Episode[]
}

type Episode = {
  episode_id: number,
  title: string,
  title_japanese: string,
  title_romanji: string,
  aired: Date,
  filler: boolean,
  recap: boolean,
  video_url: string,
  forum_url: string
}

export type SearchResponse = JikanResponse & {
  results: SearchResult[]
}

export type TopResponse = JikanResponse & {
  top: SearchResult[]
}

type SearchResult = {
  mal_id: number,
  url: string,
  image_url: string,
  title: string,
  airing: boolean,
  synopsis: string,
  type: string,
  episodes: number,
  score: number,
  start_date: Date,
  end_date: Date,
  members: number,
  rated: AgeRatings
}

export type AgeRatings = 'g' | 'pg' | 'pg13' | 'r17' | 'r' | 'rx';

export type MalReference = {
  mal_id: number,
  type: string,
  name: string,
  url: string
}
