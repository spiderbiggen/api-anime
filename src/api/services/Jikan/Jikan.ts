import Axios, { AxiosInstance, AxiosRequestConfig } from 'axios';


class Jikan {
  private _axios?: AxiosInstance;
  private get axios(): AxiosInstance {
    return this._axios || this.create();
  }

  async get<T = any>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.axios.get<T>(url, config)).data;
  }

  async post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return (await this.axios.post<T>(url, data, config)).data;
  }

  private create() {
    this._axios = Axios.create({
      baseURL: 'https://api.jikan.moe/v3/',
    });
    return this.axios;
  }
}

export default new Jikan();



