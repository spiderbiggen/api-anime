import Axios, { AxiosInstance, AxiosRequestConfig } from 'axios';


class Nyaa {
  private _axios?: AxiosInstance;
  private get axios(): AxiosInstance {
    return this._axios || this.create();
  }

  async get<T = any>(config?: AxiosRequestConfig): Promise<T> {
    let response = await this.axios.get<T>('', config);
    return response.data;
  }

  private create() {
    this._axios = Axios.create({
      baseURL: 'https://nyaa.si/rss',
    });
    this.axios.interceptors.request.use(
      async (config) => {
        config.params = Object.assign({ c: '1_2', f: 2 }, config.params);
        return config;
      },
      async error => {
        throw error;
      },
    );
    return this.axios;
  }
}

export default new Nyaa();



