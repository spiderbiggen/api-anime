import {Request, Response} from 'express';
import {Authentication, BadRequestError, LOGGER, toBoolean, TokenDetails, TokenPayload, UnauthorizedError} from '../../util';
import {User} from '../models';
import {RefreshToken} from '../models/RefreshToken';
import {RefreshTokenDetails} from '../../util/Authentication';
import moment from 'moment';

const ALLOW_AUTO_REFRESH = toBoolean(process.env.ALLOW_AUTO_REFRESH);

/**
 * Module that has all resources for managing users.
 *
 * @author Stefan Breetveld
 */
export module UserController {

  export async function self(req: Request, res: Response): Promise<any> {
    const {user}: { user?: string } = res.locals;
    return User.repository.findOne(user);
  }

  /**
   * Try to authenticate the user with the credentials stored in the body.
   *
   * {
   *     "email": "<email>",
   *     "password": "<password>"
   * }
   *
   * @param req the request sent by the client.
   */
  export async function loginUser(req: Request): Promise<TokenPayload> {
    const { email, password }: { email?: string, password?: string } = req.body;
    if (!email || !password) {
      LOGGER.debug(`Authentication failed no email or password`);
      throw new BadRequestError();
    }
    const user = await User.authorize(email, password);
    if (!user || !user.id) {
      LOGGER.verbose(`User(${ email }) failed to log in`);
      throw new UnauthorizedError('email and password do not match');
    }
    const token = Authentication.signToken(new TokenDetails({id: user.id}));
    const refreshToken: string = await generateRefreshToken(user);
    LOGGER.debug(`User(${email}) logged in`);
    return {token: token, refresh_token: refreshToken};
  }

  /**
   * Try to authenticate the user with the credentials stored in the body.
   *
   * {
   *     "refresh": "<refresh>",
   * }
   *
   * @param req the request sent by the client.
   */
  export async function refresh(req: Request): Promise<TokenPayload> {
    const {refresh}: { refresh?: string } = req.body;
    if (!refresh) {
      LOGGER.debug(`Refresh failed no refresh token`);
      throw new BadRequestError();
    }
    const tokenRepo = RefreshToken.repository;
    const refreshToken = await tokenRepo.findOne({where: {token: refresh}, relations: ['user']});
    if (!refreshToken || refreshToken.disabled) {
      LOGGER.verbose(`Refresh failed`);
      throw new UnauthorizedError('no valid refresh token');
    }
    const user = refreshToken.user;
    const token = Authentication.signToken(new TokenDetails({id: user.id}));
    LOGGER.debug(`User(${user.email}) refreshed their token`);
    if (ALLOW_AUTO_REFRESH && moment(refreshToken.expires).isAfter(moment().subtract(30, 'minutes'))) {
      return {token: token, refresh_token: await generateRefreshToken(user)};
    }
    return {token: token};
  }

  async function generateRefreshToken(user: User): Promise<string> {
    const tokenRepo = RefreshToken.repository;
    const details = new RefreshTokenDetails({id: user.id});
    const expires = moment().add(details.maxAge, 'seconds');
    let iterations = 0;
    let token: string | null;
    do {
      token = Authentication.signToken(details);
      try {
        await tokenRepo.save(tokenRepo.create({
          token: token,
          user: user,
          expires: expires.toDate()
        }));
      } catch (e) {
        LOGGER.error(JSON.stringify(e));
        LOGGER.error(e.code);
        if (e.code !== 'ER_DUP_ENTRY' || iterations++ > 5) {
          throw e;
        }
        token = null;
      }
    } while (!token);
    return token;
  }


  export async function register(req: Request): Promise<User> {
    const { email, password, username } = req.body;
    const user = await User.createUser(username, email, password);
    LOGGER.info(`Created user: ${ username }, ${ email }`);
    return user;
  }
}
