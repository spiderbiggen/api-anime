import Nyaa from '../services/Nyaa/Nyaa';
import parser from 'fast-xml-parser';
import { Request } from 'express';

export module NyaaController {
  export async function getLatestUploads() {
    const data = await Nyaa.get();
    const { rss: { channel: { item } } } = parser.parse(data);
    return item;
  }

  export async function getXPages(req: Request) {
    const { page } = req.query;
    const data = await Nyaa.get({ params: { p: Math.max(0, Number(page || 0)) } });
    const { rss: { channel: { item } } } = parser.parse(data, { ignoreNameSpace: true });
    return item;
  }

  // async function mapDownloads()
}
