/**
 * Module that has all resources for managing users.
 *
 * @author Stefan Breetveld
 */
import { Request } from 'express';
import { BadRequestError, computePercentageLevenshteinDistance, LOGGER } from '../../util';
import Jikan from '../services/Jikan/Jikan';
import { Anime, getAnimeRepository } from '../models/Anime';
import { AnimeResponse, SearchResponse, TopResponse } from '../services/Jikan/types';
import { getEpisodeRepository } from '../models/Episode';
import moment from 'moment';
import { In } from 'typeorm';

export module AnimeController {
  export async function find(req: Request) {
    const { title, page = 1, order_by, sort } = req.query;
    if (isNaN(page)) {
      throw new BadRequestError('Page query parameter must be a number');
    }
    const filter = {
      q: title,
      page: Math.max(1, page),
      order_by,
      sort,
      limit: 10,
    };
    try {
      const result = await Jikan.get<SearchResponse>('search/anime', { params: filter });
      LOGGER.debug(`${ result.results?.length } matching anime found for:"${ title }"`);
      return getAnimes(result.results.map(anime => anime.mal_id));
    } catch (e) {
      LOGGER.error(e);
      throw new BadRequestError(e);
    }
  }

  export async function get(req: Request) {
    return getAnime(Number(req.params.id));
  }

  export async function getPopular(req: Request) {
    try {
      const result = await Jikan.get<TopResponse>('top/anime/1/airing');
      return Promise.all(result.top.map(anime => getAnime(anime.mal_id)));
    } catch (e) {
      // LOGGER.error(e);
      throw new BadRequestError(e);
    }
  }

  async function getAnimes(malIds: number[]): Promise<Anime[]> {
    const repository = getAnimeRepository();
    const animes: Anime[] = await repository.find({ mal_id: In(malIds) });
    if (animes.length != malIds.length) {
      const ids = malIds.filter(id => !animes.find(anime => anime.mal_id === id));
      for (const id of ids) {
        try {
          const response = await Jikan.get<AnimeResponse>(`anime/${ id }`);
          animes.push(await mapAnime(response, repository));
        } catch (e) {
          LOGGER.error(`Error in getAnimes for anime with ${ id }: `, e);
          break;
        }
      }
    }
    animes.sort((a, b) => malIds.indexOf(a.mal_id) - malIds.indexOf(b.mal_id));
    return animes;
  }

  async function getAnime(malId: number): Promise<Anime & { [key: string]: any }> {
    const repository = getAnimeRepository();
    let anime: Anime & { [key: string]: any };
    try {
      anime = await repository.findOneOrFail({ mal_id: malId });
    } catch (e) {
      const response = await Jikan.get<AnimeResponse>(`anime/${ malId }`);
      anime = await mapAnime(response, repository);
    }
    return anime;
  }

  async function getSlug(title: string) {
    return [];
    const data = [];
    data.forEach((dat: any) => {
      const titles: string[] = Array.from(dat.alternate_titles || []);
      titles.push(dat.title);
      dat.distance = titles
        .filter(t => t)
        .map(cur => ({ key: cur, value: computePercentageLevenshteinDistance(cur, title) }))
        .sort((a, b) => b.value - a.value)[0];
    });
    return data.sort((a: any, b: any) => b.distance.value - a.distance.value);
  }

  async function mapAnime(response: AnimeResponse, repository = getAnimeRepository()): Promise<Anime> {
    let slugs: string[] | undefined = undefined;
    try {
      slugs = await getSlug(response.title);
    } catch (e) {
      LOGGER.error(e);
    }
    // @ts-ignore
    const { slug, id } = slugs && slugs.length > 0 && slugs[0].distance.value > .90 ? slugs[0] : {};

    const obj = repository.create({
      mal_id: response.mal_id,
      from: moment(response.aired.from).toDate(),
      to: response.aired.to ? moment(response.aired.to).toDate() : moment(0).toDate(),
      image: response.image_url,
      title: response.title,
      title_english: response.title_english,
      title_japanese: response.title_japanese,
      synopsis: response.synopsis,
      animeflix_slug: slug,
      animeflix_id: id,
      mal_url: response.url,
    });
    const anime = await repository.save(obj);
    anime.afterLoad();
    //@ts-ignore
    anime.slugs = slugs;
    return anime;
  }

  async function getEpisode(malId: number, episodeId: number) {
    const animeRepository = getAnimeRepository();
    const episodeRepository = getEpisodeRepository();
    // try {
    //   return await episodeRepository.findOneOrFail({ anime: malId });
    // } catch (e) {
    //   const response = await Jikan.get<AnimeResponse>(`anime/${ malId }`);
    //   const obj = episodeRepository.create({
    //     mal_id: response.mal_id,
    //     from: response.aired.from,
    //     to: response.aired.to,
    //     image: response.image_url,
    //     title: response.title,
    //     title_english: response.title_english,
    //     title_japanese: response.title_japanese,
    //     mal_url: response.url,
    //   });
    //   return episodeRepository.save(obj);
    // }
  }

}
